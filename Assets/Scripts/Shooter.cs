using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shooter : MonoBehaviour
{


    public Rigidbody bullet;
    public Transform bulletSpawnPoint;
    public float force = 200f;

    public bool shooterIsActive = true;
    private float timer = 0f;
    public float cooldown = 2f;

    private Vector3 targetAim = Vector3.zero;

    public void SetCooldown(Slider slider)
    {
        float fireRate = slider.value;
        cooldown = 2.5f - slider.value;
    }


    void Start()
    {
        transform.LookAt(targetAim);
        transform.RotateAround(transform.position, transform.right, 25);
    }

    public void ToggleActive()
    {
        if (shooterIsActive)
        {
            shooterIsActive = false;
        }
        else
        {
            shooterIsActive = true;
        }
        
    }

    void FixedUpdate()
    {
        //Shoot
        timer += Time.fixedDeltaTime;
        if (timer > cooldown)
        {
            Shoot();
            timer = 0f;
        }

        //Rotate
        transform.RotateAround(transform.position, 3*transform.up + transform.forward, 40 * Time.fixedDeltaTime);
    }

    void Shoot()
    {

        Rigidbody bulletSpawned = Instantiate(bullet, bulletSpawnPoint.position, Quaternion.identity);
        bulletSpawned.AddForce(transform.up * force, ForceMode.Impulse);
    }

}
