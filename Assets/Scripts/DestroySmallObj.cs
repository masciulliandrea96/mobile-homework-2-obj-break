using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroySmallObj : MonoBehaviour
{
    // Obj starts with initial invulnerability
    private bool invulnerable = true;
    private float invulnerabilityTimer = 0f;
    private float invulnerabilityTime = 0.2f;

    void FixedUpdate()
    {
        // Deplete invulnerability with time
        if (invulnerable)
        {
            invulnerabilityTimer += Time.fixedDeltaTime;
            if (invulnerabilityTimer > invulnerabilityTime)
            {
                invulnerable = false;
            }
        }

    }


    private void OnCollisionEnter(Collision collision)
    {
        if (!collision.gameObject.CompareTag("Floor") && invulnerable == false)
        {
            Destroy(gameObject);
        }
    }

}
