using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderTextChange : MonoBehaviour
{
    Text textComponent;
    // Start is called before the first frame update
    void Start()
    {
        textComponent = GetComponent<Text>();
    }

    // Update is called once per frame
    public void WhenSliderChanges(Slider slider)
    {
        textComponent.text = string.Format("{0:G}", slider.value);
    }
}
