using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Class destroyes object if inactive (no collisions) for too long 
public class DeathTimer : MonoBehaviour
{
    private float deathTimer = 0f;
    public float deathTime = 3f;

    void FixedUpdate()
    {
        deathTimer += Time.fixedDeltaTime;
        if (deathTimer > deathTime)
        {
            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        //reset death timer
        deathTimer = 0;
    }

}
