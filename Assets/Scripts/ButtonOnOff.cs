using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonOnOff : MonoBehaviour
{
    public Shooter shooter;
    public Text textComponent;


    public void OnButtonClick()
    {
        if (shooter.enabled)
        {
            shooter.enabled = false;
            textComponent.text = "Inactive";
        }
        else
        {
            shooter.enabled = true;
            textComponent.text = "Active";
        }

        
        
    }
}
